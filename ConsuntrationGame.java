/* Name: Tirth Darji
   Subject: Assignment 8
*/

//import java.util.*;
import java.util.ArrayList;
import java.util.Random;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class ConsuntrationGame extends JFrame
{

	private Container contents;
	public JButton [][] tiles;
	public JButton [][] bTiles;
	public JButton blankTiles;
	public JButton startOver;
	private ArrayList<ImageIcon> image;
	private ArrayList<Integer> randNum;
	public int side;
	public JButton pic1= new JButton(),pic2= new JButton();
	public int clickCount=0,trycount=0,matchcount=0,firsti=0,firstj=0,seci=0,secj=0,second=0;
	public boolean matched =false, catched = false;
	public ButtonHandler bh = new ButtonHandler();
	int  imageNumbers[][];
	public JPanel gamePanel, resultPanel;
	public JLabel lableCount, lableTime;
	public Timer timer;


	private ConsuntrationGame(int s)
	{
		super("Employee Consuntration Game");
		contents = getContentPane();
		contents.removeAll();
		side=s;
		contents.setLayout(null);
		gamePanel = new JPanel ();
		resultPanel = new JPanel ();
		gamePanel.setLocation(0,0);
		gamePanel.setSize(800,600);
		resultPanel.setLocation(0,600);
		resultPanel.setSize(800,200);
		gamePanel.setLayout(new GridLayout(side,side));
		resultPanel.setLayout(new GridLayout());


		image = new ArrayList<ImageIcon>();
		tiles = new JButton [side][side];
		bTiles = new JButton [side][side];
		randNum = new ArrayList<Integer>();
		blankTiles = new JButton();
		startOver = new JButton("Start Over");
		imageNumbers = new int [side][side];
		lableCount = new JLabel ("Number of Tries : 0");
		resultPanel.add(lableCount);
		lableTime = new JLabel ("Second counter : 0" );
		resultPanel.add(lableTime);
		resultPanel.add(startOver);
		startOver.addActionListener(bh);



		image.add(new ImageIcon("skype.png"));
		image.add(new ImageIcon("gaana.png"));
		image.add(new ImageIcon("saavn.png"));
		image.add(new ImageIcon("linkedin.png"));
		image.add(new ImageIcon("twitter.png"));
		image.add(new ImageIcon("amazon.png"));
		image.add(new ImageIcon("android.png"));
		image.add(new ImageIcon("audi.png"));
		image.add(new ImageIcon("acura.png"));
		image.add(new ImageIcon("infiniti.png"));
		image.add(new ImageIcon("honda.png"));
		image.add(new ImageIcon("toyota.png"));
		image.add(new ImageIcon("java.png"));
		image.add(new ImageIcon("acer.png"));
		image.add(new ImageIcon("dell.png"));
		image.add(new ImageIcon("facebook.png"));
		image.add(new ImageIcon("apple.png"));
		image.add(new ImageIcon("samsung.png"));
		image.add(new ImageIcon("skype.png"));
		image.add(new ImageIcon("gaana.png"));
		image.add(new ImageIcon("saavn.png"));
		image.add(new ImageIcon("linkedin.png"));
		image.add(new ImageIcon("twitter.png"));
		image.add(new ImageIcon("amazon.png"));
		image.add(new ImageIcon("android.png"));
		image.add(new ImageIcon("audi.png"));
		image.add(new ImageIcon("acura.png"));
		image.add(new ImageIcon("infiniti.png"));
		image.add(new ImageIcon("honda.png"));
		image.add(new ImageIcon("toyota.png"));
		image.add(new ImageIcon("java.png"));
		image.add(new ImageIcon("acer.png"));
		image.add(new ImageIcon("dell.png"));
		image.add(new ImageIcon("facebook.png"));
		image.add(new ImageIcon("apple.png"));
		image.add(new ImageIcon("samsung.png"));

		Random rand = new Random();
		int tempRand;

		for(int i=0 ; i<side ; i++)
		{
			for(int j=0 ; j<side ; j++)
			{
				tempRand = rand.nextInt(36);
				while(randNum.contains(tempRand))
					tempRand = rand.nextInt(side*side);
				randNum.add(tempRand);
				tiles[i][j] = new JButton (image.get(tempRand));
				imageNumbers[i][j] = tempRand % 18;
				gamePanel.add(bTiles[i][j] = new JButton());
				bTiles[i][j].addActionListener(bh);

			}
		}
		setSize(800,800);
		setVisible(true);
		contents.add(gamePanel);
		contents.add(resultPanel);

	}

	public boolean TryToPlay(int i, int j)
	{
		clickCount++;
		if (clickCount==1)
			{
				timer = new Timer (1000,bh);
				timer.start();
				firsti = i;
				firstj = j;
				bTiles[i][j].removeActionListener(bh);
				bTiles[i][j].setIcon(tiles[i][j].getIcon());
				return false;
			}
		else if (clickCount%2 == 0)
			{
				trycount++;
				seci = i;
				secj = j;
				bTiles[i][j].removeActionListener(bh);
				bTiles[i][j].setIcon (tiles[i][j].getIcon());
				lableCount.setText("Number of Tries : " + trycount);
				if (matchcount == 17)
				{
					timer.stop();
					int n = JOptionPane.showConfirmDialog(null,"You Won. \nTotal Attempts: " + trycount + "\nTotal Time "
									+ second + " seconds \nWant to play again?","Congratulation!!!",JOptionPane.YES_NO_OPTION );
					if(n == JOptionPane.YES_OPTION)
					{
						SetUpGame();
					}
					else
					{
						System.exit(0);
					}
				}
				return false;
			}
		else
			{
				if (ImageCompare(firsti,firstj,seci,secj))
				{
					matchcount++;
					firsti = i;
					firstj = j;
					bTiles[i][j].removeActionListener(bh);
					bTiles[i][j].setIcon (tiles[i][j].getIcon());
					return true;
				}
				else
				{
					bTiles[firsti][firstj].setIcon(null);
					bTiles[seci][secj].setIcon(null);
					bTiles[firsti][firstj].addActionListener(bh);
					bTiles[seci][secj].addActionListener(bh);
					firsti = i;
					firstj = j;
					bTiles[i][j].removeActionListener(bh);
					bTiles[i][j].setIcon (tiles[i][j].getIcon());
					return false;
				}
			}
	}

	public boolean ImageCompare(int fi, int fj, int si, int sj)
	{
		return imageNumbers[fi][fj] == imageNumbers[si][sj];

	}

	public int getSide(){return side;}

	private class ButtonHandler implements ActionListener
	{
		public void actionPerformed (ActionEvent ae )
		{
			if(ae.getSource() == timer)
			{
				second++;
				lableTime.setText("Second counter : " + second );
			}

			if(ae.getSource() == startOver)
			{
				SetUpGame();
			}


			for(int i=0 ; i<getSide() ; i++)
			{
				for(int j=0 ; j<getSide() ; j++)
				{
					if (ae.getSource() == bTiles[i][j])
					{
						if (TryToPlay(i,j))
							matched = true;
					}
				}
			}
		}
	}

	public void SetUpGame()
	{
		setVisible(false);
		ConsuntrationGame cg = new ConsuntrationGame(side);
	}


	public static void main (String [] args)
	{
		ConsuntrationGame cg = new ConsuntrationGame(6);
		cg.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

}